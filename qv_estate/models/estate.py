from odoo import models,fields

class Estate(models.Model):

    _name = 'estate.property'
    _description = 'estate Model'
    
    
    name = fields.Char(string='Name')
    description= fields.Text(string='Description')
    postcode=fields.Char(string='PostCode')
    data_availability=fields.Date(string='Date')
    expected_price=fields.Float(string='Expected Price')
    selling_price=fields.Float(string='Selling Price')
    bedrooms=fields.Float(string='Bedrooms')
    living_area=fields.Integer(string='Living Area')
    facades =fields.Integer(string='Facades')
    garage=fields.Boolean(string='Garage')
    garden=fields.Boolean(string='Garden')
    garden_area=fields.Integer(string='Garden Area')
    garden_orientation=fields.Selection([
        ('North','North'),
        ('South','South'),
        ('East','East'),
        ('West','West'),
    ])