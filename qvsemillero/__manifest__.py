{
 'name': 'Semilleros',
 'description': 'Semilleros',
 'summary': 'Semilleros',
 'author': 'sebas-777',
 'license': 'LGPL-3',
 'depends': [
    'base', 'hr'
 ],
 'data': [ 
      'views/semilleros_view.xml',
      'views/semillero_empleado.xml'
    ],
 'auto_install': False,
 'application': False,
}
