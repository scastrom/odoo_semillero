from odoo import models, fields


class QVSemilleros(models.Model):
    _inherit = 'hr.employee'

    blood_group = fields.Selection([
    ('OP', 'O+'),
    ('ON', 'O-'),
    ('BP', 'A+'),
    ('BN', 'A-')], 'Blood Group')
    
    city = fields.Char('City')
    
    idioma = fields.Char('Idioma')
        