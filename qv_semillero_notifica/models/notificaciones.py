from odoo import models,fields

class QVNotif(models.Model):
    _name='semilleros.notificaciones'
    
    destinatario = fields.Char('Destinatario') 
    asunto =  fields.Char('asunto')
    cuerpo =  fields.Text('cuerpo')
    
    sem_res_user = fields.Many2one('res.users','Usuarios')
    sem_hr_employee = fields.Many2one('hr.employee','Usuarios')
    
    _rec_name = 'sem_res_user'